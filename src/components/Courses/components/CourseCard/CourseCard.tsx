import React from 'react';
import Button from '../../../Header/components/Button/Button';

interface CourseCardProps {
    course: {
      id: string;
      title: string;
      description: string;
      duration: number;
      creationDate: string;
      authors: string[];
    }
  }

const CourseCard: React.FC<CourseCardProps> = (props, Button) => {
    return (
      <div className="course-card">
        <h2>{props.course.title}</h2>
        <p>Duration: {props.course.duration}</p>
        <p>Created: {props.course.creationDate}</p>
        <p>{props.course.description}</p>
        <p>Authors: {props.course.authors}</p>
        <Button buttonName="Show course" onClick= {() => {}}> </Button>
      </div>
    );
  };
  
  export default CourseCard;