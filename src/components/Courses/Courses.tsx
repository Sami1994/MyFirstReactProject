import React, { useState } from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import CourseInfo from '../CourseInfo/CourseInfo';
import Button from '../Header/components/Button/Button';
import EmptyCourseList from '../EmptyCourseList/EmptyCourseList';

const Courses: React.FC = () => {
    
    return (
      <div className="courses">
        <searchBar></searchBar>
        <CourseCard></CourseCard>
        <Button buttonName="Add New Course" onClick={() => {}} />
        
      </div>
    );
  };
  
  export default Courses;