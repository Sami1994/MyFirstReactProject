import React from 'react';
import styles from './Header.module.css';
import Logo from "./components/Logo/Logo";
import Button from './components/Button/Button';

const Header: React.FC = () => {
  return (
    <header>
      <Logo></Logo>
      <Button buttonName= "Logout" onClick = {() => {}}></Button>
    </header>
  )
}

export default Header;