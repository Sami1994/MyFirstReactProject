import React from 'react'
import AddLogo from './logo.png'

const Logo: React.FC = () => {
    return <img src = {AddLogo} alt = "Image"/>
}

export default AddLogo;