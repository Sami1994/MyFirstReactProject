import React from 'react'

interface ButtonProps {
    buttonName: string;
    onClick: () => void;
}

const Button: React.FC<ButtonProps> = (props) => {
    return <button>{props.buttonName}</button>
}

export default Button;