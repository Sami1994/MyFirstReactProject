// src/components/CourseInfo.tsx
import React from 'react';

interface CourseInfoProps {
  course: {
    id: string;
    title: string;
    description: string;
    duration: number;
    creationDate: string;
    authors: string[];
  };
  onBack: () => void;
}

const CourseInfo: React.FC<CourseInfoProps> = ({ course, onBack }) => {
  return (
    <div className="course-info">
      <button onClick={onBack}>Back</button>
      <h2>{course.title}</h2>
      <p>Duration: {course.duration}</p>
      <p>Created: {course.creationDate}</p>
      <p>{course.description}</p>
      <p>Authors: {course.authors}</p>
      <p>ID: {course.id}</p>
    </div>
  );
};

export default CourseInfo;