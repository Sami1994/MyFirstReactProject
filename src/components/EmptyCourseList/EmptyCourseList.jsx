import React from 'react';
import Button from './Button';

const EmptyCourseList: React.FC = () => {
  return (
    <div className="empty-course-list">
      <h2>Course List is Empty</h2>
      <p>Please use "Add New Course" button to add your first course.</p>
      <Button buttonText="Add New Course" onClick={() => {}} />
    </div>
  );
};

export default EmptyCourseList;